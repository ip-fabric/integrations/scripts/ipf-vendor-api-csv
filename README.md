# Add IPF Vendors via CSV

 This script will add [Vendor APIs](https://docs.ipfabric.io/latest/IP_Fabric_Settings/Discovery_and_Snapshots/Discovery_Settings/Vendors_API/) to IP Fabric via CSV File.

 **Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

# Requirements:
 - Python 3.8+
 - IP Fabric Python SDK 
 - IP Fabric API Token or Username/Password

# Install IP Fabric Python SDK:
```shell
pip install -U -r requirements.txt
```
or
```shell
pip install -U -r requirements.txt
```
# Set up .env file:
```shell
cat .env
IPF_TOKEN='mylongtokenstring123456789'
IPF_URL='https://ipfabric.is.awesome'
```
Checkout the [IP Fabric Python SDK](https://gitlab.com/ip-fabric/integrations/python-ipfabric#environment) for more information on how to set up the .env file.

# Usage:
```shell
python main.py --help

usage: main.py [-h] [--get-all-aws-vendors] [--add-from-file] [--delete-from-file] [--prep-csv]

Add or delete AWS vendor APIs

options:
  -h, --help            show this help message and exit
  --get-all-aws-vendors Get all Configured AWS vendor APIs
  --add-from-file       Add AWS vendor APIs from file
  --delete-from-file    Delete AWS vendor APIs from file
  --prep-csv            Prepare CSV file for AWS vendor APIs
```

# Examples:
## First Prep the csv with the correct headers
```shell
python main.py --prep-csv
```
## A file called aws_vendors_api.csv, is created in the current directory.
```shell
cat aws_vendor_api.csv
slug,isEnabled,respectSystemProxyConfiguration,comment,regions,assumeRoles,apiKey,apiSecret
```
## Add new Vendor APIs to the csv and then run the following command to add them to IP Fabric.
```shell
cat aws_vendor_api.csv
slug,isEnabled,respectSystemProxyConfiguration,comment,regions,assumeRoles,apiKey,apiSecret
aws1,TRUE,FALSE,,"ap-northeast-1,eu-central-2",arn:aws:iam::012345678912:user/assume_role,someKey,SomeSecret
aws2,TRUE,FALSE,foobar,"us-west-1,us-east-2",,someKey,SomeSecret
````
## Add AWS vendor APIs from file
```shell
python main.py --add-from-file
Retrieving only loaded snapshots. To load all snapshots set `unloaded` to True.
Added Vendor API with slug aws2
Added Vendor API with slug aws1
```
## Get all Configured AWS vendor APIs
```shell
python main.py --get-all-aws-vendors
Retrieving only loaded snapshots. To load all snapshots set `unloaded` to True.
Found 3 AWS vendor APIs

cat aws_vendor_api.csv
slug,isEnabled,respectSystemProxyConfiguration,comment,regions,assumeRoles,apiKey,apiSecret,ipf_vendorAPI_id
aws1,True,False,,"ap-northeast-1,eu-central-2",arn:aws:iam::012345678912:user/assume_role,,,1331765761
aws2,True,False,foobar,"us-west-1,us-east-2",,,,1331765727
````
## Delete AWS vendor APIs from file
```shell
python main.py --delete-from-file
Retrieving only loaded snapshots. To load all snapshots set `unloaded` to True.
Deleted 1331765761
Deleted 1331765727
```
