import argparse
from ipfabric import IPFClient
from ipfabric.settings import VendorAPI, AWS, AWS_REGIONS
import csv

CSV_HEADER = ["slug", "isEnabled", "respectSystemProxyConfiguration", "comment", "regions", "assumeRoles", "apiKey", "apiSecret"]
IGNORE_FIELDS = ["refillRateIntervalMs", 'refillRate', 'maxCapacity', 'maxConcurrentRequests', 'details', 'type']


def prep_csv_file():
    with open("aws_vendor_api.csv", 'w') as fp:
        writer = csv.writer(fp)
        writer.writerow(CSV_HEADER)
    print("Created aws_vendor_api.csv")


def get_all_vendor_apis():
    ipf = IPFClient()
    vendor_apis = VendorAPI(ipf)
    data = []
    for vendor_api in vendor_apis.get_vendor_apis():
        if vendor_api['type'] == "aws-ec2":
            data.append(vendor_api)
    print(f"Found {len(data)} AWS vendor APIs")
    with open("aws_vendor_api.csv", 'w') as fp:
        field_names = [csv_header for csv_header in CSV_HEADER]
        field_names.append('ipf_vendorAPI_id')
        writer = csv.DictWriter(fp, fieldnames=field_names)
        writer.writeheader()
        for row in data:
            write_row = {'apiKey': '', 'apiSecret': ''}
            for k, v in row.items():
                if k not in IGNORE_FIELDS:
                    if k in ['regions', 'assumeRoles']:
                        if k == 'regions':
                            write_row[k] = ','.join(region for region in v)
                        if k == 'assumeRoles':
                            write_row[k] = ','.join(role['role'] for role in v)
                        continue
                    if k == 'id':
                        write_row['ipf_vendorAPI_id'] = v
                        continue
                    write_row[k] = v
            writer.writerow(write_row)


def add_vendor_api(ipf: IPFClient, aws: AWS):
    vendor_apis = VendorAPI(ipf)
    vendor_apis.add_vendor_api(aws)
    print(f"Added Vendor API with slug {aws.slug}")


def delete_vendor_api(ipf: IPFClient, vendor_id):
    vendor_apis = VendorAPI(ipf)
    vendor_apis.delete_vendor_api(vendor_id)
    print(f"Deleted {vendor_id}")


def add_vendor_apis_from_file():
    ipf = IPFClient()
    with open("aws_vendor_api.csv", 'r') as fp:
        reader = csv.DictReader(fp)
        rows = list(reader)
    vendors_too_add = list()
    for row in rows:
        vendor_data = {}
        for k, v in row.items():
            if k in ['id', 'type', 'ipf_vendorAPI_id']:
                continue
            elif k not in CSV_HEADER:
                print(f"Skipping {k}={v}, not recognized")
                continue
            elif k in ['regions', 'assumeRoles']:
                vendor_data[k] = v.split(',')
                if k == 'regions':
                    for region in vendor_data[k]:
                        if region not in AWS_REGIONS:
                            raise ValueError(f"{region} is not a valid AWS Region")
                if k == 'assumeRoles':
                    vendor_data[k] = [role for role in vendor_data[k] if role != '']
            elif k in ['apiKey', 'apiSecret']:
                if v == '':
                    raise ValueError(f"Missing {k}, cant be empty when adding a new vendor API")
                vendor_data[k] = v
            else:
                vendor_data[k] = v
        vendors_too_add.append(AWS(**vendor_data))
    for aws in vendors_too_add:
        add_vendor_api(ipf, aws)


def delete_vendor_apis_from_file():
    ipf = IPFClient()
    with open("aws_vendor_api.csv", 'r') as fp:
        reader = csv.DictReader(fp)
        rows = list(reader)
    for row in rows:
        delete_vendor_api(ipf, row['id'])


def main():
    parser = argparse.ArgumentParser(description="Add or delete AWS vendor APIs")
    parser.add_argument("--get-all-aws-vendors", action="store_true", help="Get all Configured AWS vendor APIs")
    parser.add_argument("--add-from-file", action="store_true", help="Add AWS vendor APIs from file")
    parser.add_argument("--delete-from-file", action="store_true", help="Delete AWS vendor APIs from file")
    parser.add_argument("--prep-csv", action="store_true", help="Prepare CSV file for AWS vendor APIs")
    args = parser.parse_args()
    if args.get_all_aws_vendors:
        get_all_vendor_apis()
    elif args.add_from_file:
        add_vendor_apis_from_file()
    elif args.prep_csv:
        prep_csv_file()
    elif args.delete_from_file:
        delete_vendor_apis_from_file()


if __name__ == "__main__":
    main()

